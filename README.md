# Agrostand Business - Agronomist

At Agrostand , we’re transforming the Agri-value chain through technology. For farm owners, we enable digital access to a wider marketplace. For agribusinesses , we assure quality whilst mitigating counterparty risk and enabling secure payments. We also provide last mile pick from Agri-businesses and last mile pickup to Farmers with our Agro Delivery Facility.

# OBJECTIVES

Our platform provides access to agricultural inputs such as fertilizers, seeds, planting materials, and equipment at the touch of a button and farmers can create their own trade profiles and directly deal with agribusinesses .
On our app, farmers and agri-businesses can create their own trade terms and directly negotiate with agribusinesses . We also take care of transportation/Delivery (last mile pickup & drop) and payments , to drive higher efficiency in the Agri-value chain.
Farmers & Agri-Businesses benefit from the instant payments credited to their Bank account, as soon as the goods are delivered.
We will update weather data and provide forecasts, Provide Mandi rates and share real-time farming advice.
We leverage technology to help our buyers track goods at every step on the way and ensure on the time payment to farmers.
Our easy-to-use platform is designed to allow agribusinesses to directly negotiate with farmers and realise higher transparency in their supply chain.

# PRE-REQUEST

01. Have wi-fi or mobile data internet connection
02. Android device

# FEATURES 

01. Create your E-bill and send to the particular Vendor's & Farmer's
02. Shipping process with multiple option :
    1. COD 
    2. Agro pay (powered by Agrostand)
    3. Pre-paid (to take some amount before delivering)
03. Have option to add multiple products in a single E-bill 
04. Seperate list to check the request send and recived along with there current status.
05. Chat option (FCM chat system)
06. Notification (Accourding to the language)

# GOOGLE PLAY STORE

[Agrostand Business - Agronomist](https://play.google.com/store/apps/details?id=com.ps.adc&hl=en-GB)

# SCREENSHOTS

- **Select Language**

![alt text](images/Screenshot_from_2021-07-24_10-42-34.png "Select Language Screen")


- **Login Screen**

![alt text](images/Screenshot_from_2021-07-24_10-42-46.png "Login Screen")

- **Dashboard Screen**

![alt text](images/Screenshot_from_2021-07-24_10-42-58.png "Dashboard Screen")

- **Drawer Screen**

![alt text](images/Screenshot_from_2021-07-24_10-43-06.png "Drawer Screen")

# DEVELOPER INFO

[Parkhya Solutions Pvt. Ltd.](https://www.parkhya.com/)



